var express = require('express'),
	mongoose = require('mongoose'),
	bodyParser = require('body-parser'),
	logger = require('morgan'),
	q = require('q'),
	flash = require('connect-flash'),
	session = require('express-session'),
	path = require('path'),
	handlebars = require('express-handlebars'),
	handlebars_sections = require('express-handlebars-sections');

mongoose.Promise = q.Promise;

var CONFIG = require('./config');
var app = express();


//MONGODB
//1 - Load created models
var Comment = require('./app/models/Comment'),
	Post = require('./app/models/Post');
//connect
mongoose.connect(CONFIG.DB.uri, {
		useMongoClient: true,
		reconnectTries: 30,
		reconnectInterval: 1000,
		keepAlive: 1,
		connectTimeoutMS: 30000
	})
	.then(() => {
		console.log('SUCCESS MGDB');
	})
	.catch((err) => {
		console.log(err);
	});

//CONFIG
app.use(express.static(path.join(__dirname + '/app/public')));

app.set('port', process.env.PORT || 3000);
//set views folder //default: views --> can omit
app.set('views', path.join(__dirname, '/app/views'));

//set view engine
app.engine('hbs', handlebars({
	extname: 'hbs',
	defaultLayout: 'main',
	layoutsDir: path.join(__dirname, '/app/views/_layouts/'),
	partialsDir: path.join(__dirname, '/app/views/_partials/'),
	helpers: {
		section: handlebars_sections(),
		formatCurrency: function(n) {
			return n.toFixed(2).replace(/(\d)(?=(\d{3})+\.)/g, '$1,') + "$";
		}
	}
}))
app.set('view engine', 'hbs');

app.use(session({
	key: 'session_cookie_name',
	secret: 'session_cookie_secret',
	resave: false,
	saveUninitialized: false
}));
app.use(flash());

//MIDDLEWARES
app.use(bodyParser.urlencoded({
	extended: true
}));
app.use(bodyParser.json());

app.use(logger('dev'));

//ROUTES
app.use('/ajax', require('./app/ajax-routes'));
app.use(require('./app/routes'));


//START SERVER
app.listen(app.get('port'), () => {
	if (process.env.NODE_ENV != 'test')
		console.log('API Server is listening at port ' + app.get('port'));
});

module.exports = app;