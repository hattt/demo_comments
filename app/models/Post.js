var mongoose = require('mongoose'),
	Schema = mongoose.Schema;

var postSchema = Schema({
	name: String
});


module.exports = mongoose.model('Post', postSchema)