var mongoose = require('mongoose'),
	Schema = mongoose.Schema,
	Post = require('./Post');

var commentSchema = Schema({
	post: {
		type: Schema.Types.ObjectId,
		ref: 'Post',
		required: true
	},
	text: {
		type: String,
		minlength: 3,
	},
	parent: {
		type: Schema.Types.ObjectId,
		ref: 'Comment'
	}

});

var Comment = mongoose.model('Comment', commentSchema)

//parent validate
commentSchema.path('parent').validate(function(value, respond) {

	Comment.findOne({
		_id: value
	}, function(err, doc) {
		if (err || !doc)
			respond(false);
		else
			respond(true);
	});

}, 'parent not exist');

//post validate
commentSchema.path('post').validate(function(value, respond) {

	Post.findOne({
		_id: value
	}, function(err, doc) {
		if (err || !doc)
			respond(false);
		else
			respond(true);
	});

}, 'post not exist');

module.exports = Comment;