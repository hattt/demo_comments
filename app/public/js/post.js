$(document).ready(function() {
	loacComments((function(rlt) {
		if (rlt) $('.view-more-replies').trigger('click');
	}));
});


function loacComments(cb) {
	var postId = $('#postId').text();
	$.getJSON('/ajax/comments/' + postId, function(data) {
		if (!data.success) return cb(false);

		var ul = $('.comments-list');

		var x, replies = data.data;

		replies.forEach(function(rep) {
			if (rep.num_replies > 0)
				x = $('<li data-id="' + rep._id.toString() +
					'" class="comment"> <a class="pull-left" href="#"> ' +
					'<img class="avatar" src="http://bootdey.com/img/Content/user_3.jpg" alt="avatar"> ' +
					'</a> <div class="comment-body"> <div class="comment-heading"> <h4 class="user">NAME</h4> <h5 class="time">X minutes ago</h5> </div> <p>' +
					rep.text.toString() + '</p> <span class="span-link show-reply-box">reply</span>' +
					'<div> <span class="span-link view-more-replies"><i class="fa fa-angle-double-down" aria-hidden="true"></i> ' + rep.num_replies.toString() + ' replies</span> </div> <ul class="comments-list"></ul><div class="reply-box"> </div> </div> </li>');
			else
				x = $('<li data-id="' + rep._id.toString() +
					'" class="comment"> <a class="pull-left" href="#"> ' +
					'<img class="avatar" src="http://bootdey.com/img/Content/user_3.jpg" alt="avatar"> ' +
					'</a> <div class="comment-body"> <div class="comment-heading"> <h4 class="user">NAME</h4> <h5 class="time">X minutes ago</h5> </div> <p>' +
					rep.text.toString() + '</p> <span class="span-link show-reply-box">reply</span>' +
					'<div hidden> <span class="span-link view-more-replies"><i class="fa fa-angle-double-down" aria-hidden="true"></i>replies</span></div>' +
					'<ul class="comments-list"></ul><div class="reply-box"></div></div> </li>');
			ul.append(x);
		});

		cb(true);
	});
}

$('.comments-list').on('click', '.show-reply-box', function() {
	var commentId = $(this).closest('li').data('id').toString();
	var postId = $('#postId').text().toString();
	var replyBox = $(this).closest('li').find('.reply-box').last();

	if ($(replyBox).children().length > 0) {
		$('html, body').animate({
			scrollTop: $(replyBox).children().offset().top - 100
		}, 2000);

		return;
	}

	var form = $('<form class="form-reply" action="/ajax/posts/' + postId + '/reply" method="post"> <input type="text" hidden name="parent" value="' + commentId + '"><div class="input-group"><input class="form-control" placeholder="Nhập bình luận ..." type="text" name="text"> <span class="input-group-addon  span-link send-reply"> <i class="fa fa-send"></i> </span> </div> </form>');
	replyBox.append(form);

	$('html, body').animate({
		scrollTop: $(form).offset().top - 100
	}, 2000);

});
$('.comments-list').on('click', '.view-more-replies', function() {
	var postId = $('#postId').text();
	var ul = $(this).closest('li').find('ul').first();
	$(this).hide();

	console.log(ul);
	var url = '/ajax/comments/' + postId + '/' + $(this).closest('li').data('id');
	$.getJSON(url, function(data) {

		if (!data.success) return alert(data.msg);

		ul.empty();
		var x, replies = data.data;
		console.log(replies);
		replies.forEach(function(rep) {


			if (rep.num_replies > 0)
				x = $('<li data-id="' + rep._id.toString() +
					'" class="comment"> <a class="pull-left" href="#"> ' +
					'<img class="avatar" src="http://bootdey.com/img/Content/user_3.jpg" alt="avatar"> ' +
					'</a> <div class="comment-body"> <div class="comment-heading"> <h4 class="user">NAME</h4> <h5 class="time">X minutes ago</h5> </div> <p>' +
					rep.text.toString() + '</p> <span class="span-link show-reply-box">reply</span>' +
					'<div> <span class="span-link view-more-replies"><i class="fa fa-angle-double-down" aria-hidden="true"></i> ' + rep.num_replies.toString() + ' replies</span></div><ul class="comments-list"></ul><div class="reply-box"></div> </div> </li>');
			else
				x = $('<li data-id="' + rep._id.toString() +
					'" class="comment"> <a class="pull-left" href="#"> ' +
					'<img class="avatar" src="http://bootdey.com/img/Content/user_3.jpg" alt="avatar"> ' +
					'</a> <div class="comment-body"> <div class="comment-heading"> <h4 class="user">NAME</h4> <h5 class="time">X minutes ago</h5> </div> <p>' +
					rep.text.toString() + '</p> <span class="span-link show-reply-box">reply</span>' +
					'<div hidden> <span class="span-link view-more-replies"><i class="fa fa-angle-double-down" aria-hidden="true"></i> ' + rep.num_replies.toString() + ' replies</span></div>' +
					'<ul class="comments-list"></ul><div class="reply-box"></div> </div> </li>');
			ul.append(x);
		});
	});
});

// $('#form-comment').on('keyup keypress', function(e) {
// 	var keyCode = e.keyCode || e.which;
// 	if (keyCode === 13) {
// 		e.preventDefault();
// 		return false;
// 	}
// });


//send comment: click & enter
$('.send-comment').on('click', function() {
	var form = $(this).closest('form');
	$.post($(form).attr('action'), $(form).serialize(), function(data) {
		if (!data.success) return alert('send comment false');

		var ul = $('.comments-list').first();

		var x, comment = data.data;
		x = $('<li data-id="' + comment._id.toString() +
			'" class="comment"> <a class="pull-left" href="#"> ' +
			'<img class="avatar" src="http://bootdey.com/img/Content/user_3.jpg" alt="avatar"> ' +
			'</a> <div class="comment-body"> <div class="comment-heading"> <h4 class="user">NAME</h4> <h5 class="time">X minutes ago</h5> </div> <p>' +
			comment.text.toString() + '</p> <span class="span-link show-reply-box">reply</span>' +
			'<ul class="comments-list"></ul><div class="reply-box"></div></div> </li>');
		ul.append(x);

		var container = $('body'),
			scrollTo = $(x);

		container.animate({
			scrollTop: scrollTo.offset().top - container.offset().top + container.scrollTop()
		});

	}, 'json');
});
$('#form-comment').keypress(function(e) {
	if (e.which == 13) {
		var form = $(this);
		$.post($(form).attr('action'), $(form).serialize(), function(data) {
			if (!data.success) return alert('send comment false');

			var ul = $('.comments-list').first();

			var x, comment = data.data;
			x = $('<li data-id="' + comment._id.toString() +
				'" class="comment"> <a class="pull-left" href="#"> ' +
				'<img class="avatar" src="http://bootdey.com/img/Content/user_3.jpg" alt="avatar"> ' +
				'</a> <div class="comment-body"> <div class="comment-heading"> <h4 class="user">NAME</h4> <h5 class="time">X minutes ago</h5> </div> <p>' +
				comment.text.toString() + '</p> <span class="span-link show-reply-box">reply</span>' +
				'<ul class="comments-list"></ul><div class="reply-box"></div></div> </li>');
			ul.append(x);

			var container = $('body'),
				scrollTo = $(x);

			container.animate({
				scrollTop: scrollTo.offset().top - container.offset().top + container.scrollTop()
			});

		}, 'json');
		return false; //<---- Add this line
	}
});

//send reply: click & enter
$('.comments-list').on('click', '.send-reply', function() {
	var form = $(this).closest('form');
	var viewmore = $(this).closest('li').find('.view-more-replies').first();

	$.post($(form).attr('action'), $(form).serialize(), function(data) {
		if (!data.success) return alert('send comment false');
		console.log(viewmore);
		$(viewmore).trigger('click');
		var container = $('body'),
			scrollTo = $(form);

		container.animate({
			scrollTop: scrollTo.offset().top - container.offset().top + container.scrollTop()
		});
	}, 'json');
});
$('.comments-list').on('keypress', '.form-reply', function(e) {
	if (e.which == 13) {
		debugger;
		var form = $(this);
		var viewmore = $(this).closest('li').find('.view-more-replies').first();
		$.post($(form).attr('action'), $(form).serialize(), function(data) {
			if (!data.success) return alert('send comment false');
			console.log(viewmore);
			$(viewmore).trigger('click');

			var container = $('body'),
				scrollTo = $(form);
			container.animate({
				scrollTop: scrollTo.offset().top - container.offset().top + container.scrollTop()
			});
		}, 'json');
		return false; //<---- Add this line
	}
});

// $('#list-comments').on('click', '.btnViewMore', function() {
// 	var postId = $('#postId').text();
// 	var ul = $(this).parent().find('ul');
// 	$(this).hide();

// 	console.log(ul);
// 	var url = '/ajax/comments/' + $(this).data('postid') + '/' + $(this).data('parentid');
// 	$.getJSON(url, function(data) {

// 		if (!data.success) return alert(data.msg);

// 		data.data.forEach(function(d) {
// 			if (d.num_replies > 0)
// 				x = $('<li id="' + d._id.toString() + '"><span>' + d.text.toString() + '</span><form action="/posts/' + postId.toString() +
// 					'/reply" method="post"><input type="text" name="text"><input type="text" hidden name="parent" value="' + d._id.toString() +
// 					'"><button>reply</button></form><button class="btnViewMore" data-postid="' + postId.toString() + '" data-parentid="' + d._id + '">' + d.num_replies.toString() + ' replies</button><ul></ul></li>');
// 			else
// 				x = $('<li id="' + d._id.toString() + '"><span>' + d.text.toString() + '</span><form action="/posts/' + postId.toString() +
// 					'/reply" method="post"><input type="text" name="text"><input type="text" hidden name="parent" value="' + d._id.toString() +
// 					'"><button>reply</button></form><ul></ul></li>');
// 			ul.append(x);
// 		});

// 		console.log(data.data);
// 	});
// });