var r = require('express').Router();

var mongoose = require('mongoose'),
	q = require('q'),
	Comment = mongoose.model('Comment'),
	Post = mongoose.model('Post');
mongoose.Promise = q.Promise;

r.get('/comments/:postId', (req, res, next) => {
	var postId = req.params.postId;

	Comment.find({
		post: postId,
		parent: undefined
	}).then((comments) => {

		var promises = [];
		comments.forEach((c) => {
			promises.push(Comment.find({
				parent: c._id
			}));
		});
		return [comments, q.all(promises)];
	}).spread((comments, rlt) => {
		var data = [];
		for (var i = 0; i < comments.length; i++) {
			var t = JSON.parse(JSON.stringify(comments[i]));
			t.num_replies = rlt[i].length;
			data.push(t);
		}

		res.json({
			success: true,
			data: data
		});
	}).catch((err) => {
		next(err);
	});
});
r.get('/comments/:postId/:parentId', (req, res, next) => {

	var postId = req.params.postId,
		parentId = req.params.parentId;

	Comment.find({
		post: postId,
		parent: parentId
	}).then((comments) => {

		var promises = [];
		comments.forEach((c) => {
			promises.push(Comment.find({
				parent: c._id
			}));
		});
		return [comments, q.all(promises)];
	}).spread((comments, rlt) => {
		var data = [];
		for (var i = 0; i < comments.length; i++) {
			var t = JSON.parse(JSON.stringify(comments[i]));
			t.num_replies = rlt[i].length;
			data.push(t);
		}

		res.json({
			success: true,
			data: data
		});
	}).catch((err) => {
		next(err);
	});
});

//comment & reply
r.post('/posts/:id/comment', (req, res, next) => {
	var postId = req.params.id;

	var comment = new Comment({
		text: req.body.text,
		post: postId
	});
	comment.save((err, newcomment) => {
		if (err) return next(err);

		res.json({
			success: true,
			data: newcomment
		});
	});
});
r.post('/posts/:id/reply', (req, res, next) => {
	var postId = req.params.id;

	var comment = new Comment({
		text: req.body.text,
		post: postId,
		parent: req.body.parent
	});
	comment.save((err, newcomment) => {
		if (err) return next(err);
		res.json({
			success: true,
			data: newcomment
		});
	});
});



r.use((err, req, res, next) => {
	console.log(err);
	res.status(500).send({
		success: false,
		msg: 'Error happened!'
	});
});

r.use((req, res, next) => {
	res.status(404).send({
		success: false,
		msg: 'NOT FOUND'
	});
});

module.exports = r;