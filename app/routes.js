var r = require('express').Router();

var mongoose = require('mongoose'),
	q = require('q'),
	Comment = mongoose.model('Comment'),
	Post = mongoose.model('Post');
mongoose.Promise = q.Promise;


r.get('/', (req, res, next) => {
	Post.find({})
		.then((posts) => {
			var vm = {
				posts: posts
			}
			res.render('index', vm);
		}).catch((err) => {
			next(err);
		});

});

r.get('/create', (req, res, next) => {
	var vm = {
		msg: req.flash('msg')
	}
	res.render('create', vm);
});
r.post('/create', (req, res, next) => {
	var post = new Post({
		name: req.body.name
	});
	post.save((err, newpost) => {
		req.flash('msg', 'create success!')
		res.redirect('/create');
	});
});

r.get('/posts/:id', (req, res, next) => {
	var postId = req.params.id;
	var vm = {
		msg: req.flash('msg')
	};

	Post.findOne({
			_id: postId
		})
		.then((post) => {
			debugger;
			vm.post = post;
			return Comment.find({
				post: postId,
				parent: undefined
			});

		}).then((comments) => {

			var promises = [];
			comments.forEach((c) => {
				promises.push(Comment.find({
					parent: c._id
				}));
			});
			return [comments, q.all(promises)];
		}).spread((comments, rlt) => {
			for (var i = 0; i < comments.length; i++) {
				comments[i].replies = rlt[i];
			}

			vm.comments = comments;
			res.render('post', vm);
		}).catch((err) => {
			next(err);
		});
});
r.post('/posts/:id/comment', (req, res, next) => {
	var postId = req.params.id;
	debugger;
	var comment = new Comment({
		text: req.body.text,
		post: postId
	});
	comment.save((err, newcomment) => {
		if (err) return next(err);
		req.flash('msg', 'comment success!');
		res.redirect('/posts/' + postId);
	});
});
r.post('/posts/:id/reply', (req, res, next) => {
	var postId = req.params.id;
	debugger;
	var comment = new Comment({
		text: req.body.text,
		post: postId,
		parent: req.body.parent
	});
	comment.save((err, newcomment) => {
		if (err) return next(err);
		req.flash('msg', 'reply success!');
		res.redirect('/posts/' + postId);
	});
});


r.use((err, req, res, next) => {
	console.log(err);
	res.status(500).send(err);
});

r.use((req, res, next) => {
	res.status(404).send('NOT FOUND');
});

module.exports = r;